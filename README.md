# jl-cid

#### 介绍
极简的jar自动构建、部署工具，项目代码量也是极少，以jar的形式引入项目即可，流程为 打包 --> 上传 --> 启动/重启。


#### 使用场景
1. 面向开发人员的测试服打包工具。
2. 小型公司没有专业的ci/cd产品。
3. 不需要其他功能，只是单纯想要把本地包部署到服务器。


#### 支持功能
1. 支持maven打包
2. 依赖的形式引入项目，打包当前项目，极简。
3. 打包 --> 上传 --> 启动/重启 一条线。
4. 多台服务器打包上传。


#### 安装依赖
```
<dependency>
    <groupId>io.gitee.laoshirenggo</groupId>
    <artifactId>cid</artifactId>
    <version>1.4</version>
</dependency>
```


#### 使用例子
yml配置服务器ssh信息：
```
jl:
  cid:
    ssh:
      #服务器1 shh连接信息
      - ip: 127.0.0.1
        port: 22
        user: root
        password: root
        path: /home/package             #上传到服务器的路径
        command: docker restart test    #上传完成后执行的命令
      #服务器2 shh连接信息
      - ip: 127.0.0.2
        port: 22
        user: root
        password: root
        path: /home/package             #上传到服务器的路径
        command: docker restart demo    #上传完成后执行的命令
```
启动项目，访问 http://127.0.0.1:port/index.html。

![image](https://gitee.com/laoshirenggo/jl-cid/raw/master/src/main/resources/1.png)

![image](https://gitee.com/laoshirenggo/jl-cid/raw/master/src/main/resources/2.png)



