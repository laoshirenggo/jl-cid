package com.jl.config;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * ssh配置
 */
@Data
@Component
@ConfigurationProperties(prefix = "jl.cid")
public class JLCidConfig {

    /**
     * 线程参数
     */
    private List<SSH> ssh;

    @Data
    @Accessors(chain = true)
    public static class SSH {

        /**
         * ip
         */
        private String ip;

        /**
         * 端口
         */
        private Integer port;

        /**
         * 账号
         */
        private String user;

        /**
         * 密码
         */
        private String password;

        /**
         * 上传路径
         */
        private String path;

        /**
         * 执行命令
         */
        private String command;
    }
}
