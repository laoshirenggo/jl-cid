var cur = window.document.location.href;
var pathname = window.document.location.pathname;
var pos = cur.indexOf(pathname);
var localhostPath = cur.substring(0, pos);

/**
 * 公用发送ajax请求
 * @param {Object} url
 * @param {Object} param
 * @param {Object} type 可选，类型，默认get
 */
function sendAjax(url, param, type) {
    if (type == null) {
        type = "get";
    }

    var result = null;
    $.ajax({
        url: localhostPath + "/" + url,
        data: param,
        type: type,
        async: false,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            result = data;
        }
    });
    return result;
}

/**
 * 提示层
 * @param {Object} msg
 */
function msg(msg) {
    layer.msg(msg);
}

/**
 * 加载层
 */
function load() {
    var index = layer.load(1, {
        shade: [0.5, '#fff'] //透明度的白色背景
    });
    return index;
}

/**
 * 关闭窗口
 * @param {Object} index
 */
function close(index) {
    layer.close(index);
}

/**
 * 关闭当前窗口
 */
function closes() {
    layer.close(layer.index);
}