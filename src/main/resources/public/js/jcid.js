$(document).ready(function () {
    var line;

    var method = {
        getServlet: function () {
            var result = sendAjax("jcid/getServlet");
            for (var i = 0; i < result.length; i++) {
                if (result[i].command == null) {
                    result[i].command = "";
                }
                var div = '<div class="row cl">' +
                    '<label class="form-label col-xs-4 col-sm-3">ssh(' + (i + 1) + ')：</label>' +
                    '<div class="formControls col-xs-8 col-sm-9">' +
                    '<input type="text" class="input-text" value=' + result[i].ip + ' style="width: 15%; margin-right: 8px;" disabled="disabled">' +
                    '<input type="text" class="input-text" value=' + result[i].path + ' style="width: 15%; margin-right: 8px;" disabled="disabled">' +
                    result[i].command +
                    '</div>' +
                    '</div>';
                $("#form-admin-add").append(div);
            }
        },
        cid: function () {
            sendAjax("jcid/cid");
        },
        getLine: function () {
            var result = sendAjax("jcid/getLine");
            if (!result.cid) {
                clearInterval(line);
                $("#log").children("img").remove();
            } else {
                if (result.line.indexOf("开始上传") != -1) {
                    $("#log").html(result.line + "<br><img src='/img/load.gif' style='width: 26px;height: 26px;' />");
                } else {
                    $("#log").html(result.line + "<br>");
                }
                $("#log").scrollTop($("#log")[0].scrollHeight);
            }
        }
    }
    method.getServlet();

    $("#submits").click(function () {
        layer.open({
            id: "log",
            type: 1,
            title: '构建日志',
            skin: 'layui-layer-rim', //加上边框
            area: ['800px', '600px'], //宽高
            content: ''
        });
        method.cid();
        var index = load();
        line = setInterval(function () {
            method.getLine();
            close(index);
        }, 1000);
    });
});